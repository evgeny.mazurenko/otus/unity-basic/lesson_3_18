using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerPhysics : MonoBehaviour
{
    private static readonly int IsRunning = Animator.StringToHash("IsRunning");
    private static readonly int IsJumping = Animator.StringToHash("IsJumping");

    [SerializeField]
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private SpriteRenderer _shadowSpriteRenderer;

    [SerializeField]
    private Animator _animator;


    #region Physics

    [SerializeField]
    private Rigidbody2D _rigidbody2D;

    [SerializeField]
    private Collider2D _collider2D;

    [SerializeField]
    private Collider2D _feetCollider2D;

    [SerializeField]
    private float _speed;

    [SerializeField]
    private float _verticalSpeed;

    [SerializeField] 
    private float _wallJumpSpeed;

    [SerializeField] 
    private float _wallJumpingTimeout;

    [SerializeField]
    private LayerMask _groundLayer;
    
    [SerializeField]
    private LayerMask _wallLayer;

    private bool _isJumping;

    private float _jumpWallTime;

    private ContactFilter2D _groundContactFilter;
    
    private ContactFilter2D _wallContactFilter;

    #endregion

    private void Start()
    {
        _groundContactFilter = new ContactFilter2D();
        _groundContactFilter.SetLayerMask(_groundLayer);
        //Теперь прыжок возможен только с горизнтальной поверхности
        _groundContactFilter.SetNormalAngle(90,90);
        
        _wallContactFilter = new ContactFilter2D();
        _wallContactFilter.SetLayerMask(_wallLayer);
    }

    private void Update()
    {
        // Walking/Running
        var horizontal = Input.GetAxis("Horizontal");
        if (horizontal != 0)
        {
            if (!_isJumping)
            {
                _animator.SetBool(IsRunning, true);
            }
            
            bool isMovingToRight;
            if (horizontal > 0f)
            {
                isMovingToRight = true;
            }
            else 
            {
                isMovingToRight = false;
            }
            
            _spriteRenderer.flipX = !isMovingToRight;
            
            // Чтобы не было прилипания к вертикальным поверхностям
            // горизонтальное перемещение блокируется при сонаправленном контакте со стеной 
            if (!(_isJumping && 
                  HasContactWithWall(out Vector2 wallContactNormal) && 
                  IsNormalContactAlongDirection(isMovingToRight, wallContactNormal))
                )
                _rigidbody2D.velocity = new Vector2((isMovingToRight ? 1:-1) * _speed, _rigidbody2D.velocity.y);
        }
        else
        {
            _animator.SetBool(IsRunning, false);
        }

        // Jumping
        var vertical = Input.GetAxis("Vertical");
        if (vertical > 0f && IsOnGround())
        {
            _animator.SetBool(IsJumping, true);

            _shadowSpriteRenderer.enabled = false;

            _isJumping = true;

            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _verticalSpeed);
        }
        else if (vertical > 0f && 
                 HasContactWithWall(out Vector2 wallContactNormal) &&
                 // Защита от бега по стенам
                 Time.time - _jumpWallTime > _wallJumpingTimeout &&
                 // Запрет прыжка от стены с поверхности
                 !IsOnGround()
                )
        {
            _rigidbody2D.velocity = new Vector2(wallContactNormal.x * _wallJumpSpeed, _verticalSpeed);
            _jumpWallTime = Time.time;
        }
        else if (IsOnGround())
        {
            _animator.SetBool(IsJumping, false);

            _isJumping = false;

            _shadowSpriteRenderer.enabled = true;
        }
    }

    private bool HasContactWithWall(out Vector2 contactNormal)
    {
        contactNormal = Vector2.up;
        List<ContactPoint2D> contacts = new();
        _feetCollider2D.GetContacts(_wallContactFilter, contacts);
        if (contacts.Count > 0)
        {
            contactNormal = contacts[0].normal;
            return true;
        }
        return false;
    }

    private bool IsNormalContactAlongDirection(bool isToRight, Vector2 normal)
    {
        return (isToRight && normal == Vector2.left) || (!isToRight && normal == Vector2.right);
    }
    
    private bool IsOnGround()
    {
        List<ContactPoint2D> contacts = new();
        return _feetCollider2D.GetContacts(_groundContactFilter, contacts) > 0;
    }

    
}