using UnityEngine;

public sealed class PlatformMoving : MonoBehaviour
{
    private const float Epsilon = 0.02f;

    [SerializeField] private Rigidbody2D rb;

    [SerializeField] private Transform[] routePositions;

    [SerializeField] private float speed = 1f;

    private int _nextPositionIdx = -1;

    private Vector2 _nextPosition;

    private Vector2 _toNextDirection;

    private void Start()
    {
        InitNextPosition();
    }

    private void InitNextPosition()
    {
        _nextPositionIdx = (_nextPositionIdx == routePositions.Length - 1) ? 0 : _nextPositionIdx + 1;
        _nextPosition = routePositions[_nextPositionIdx].position;
        _toNextDirection = (_nextPosition - rb.position).normalized;
    }

    private void FixedUpdate()
    {
        float distanceToNewPosition = Vector2.Distance(_nextPosition, rb.position);
        if (distanceToNewPosition > Epsilon)
        {
            Vector2 newPosition = rb.position + _toNextDirection * speed * Time.fixedDeltaTime;
            rb.MovePosition(newPosition);
        }
        else
        {
            InitNextPosition();
        }
    }
}