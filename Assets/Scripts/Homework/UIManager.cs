using UnityEngine;

public sealed class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject sceneUI;
    
    void Start()
    {
        sceneUI.SetActive(false);
    }

    public void ShowFinishView()
    {
        sceneUI.SetActive(true);
    }
}
