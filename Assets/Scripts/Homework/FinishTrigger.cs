using UnityEngine;

public sealed class FinishTrigger : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;

    private void OnTriggerEnter2D(Collider2D other)
    {
        uiManager.ShowFinishView();
    }
}